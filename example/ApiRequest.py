import re

branchName = %teamcity.build.branch%
vcsRoot = %vcsroot.url%

urlGitlabMR = re.sub("\\.git$", "", vcsRoot)
urlGitlabMR += "/-/merge_requests/" + branchName
print(urlGitlabMR)
